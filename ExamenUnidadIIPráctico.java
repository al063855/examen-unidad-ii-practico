package examen.unidad.ii.práctico;

import java.util.Scanner;

/**
 *
 * @author hitzu
 */
public class ExamenUnidadIIPráctico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String Alfabeto [][] = new String [26][2];
        Alfabeto[0][0] = "A";
        Alfabeto[1][0] = "B";
        Alfabeto[2][0] = "C";
        Alfabeto[3][0] = "D";
        Alfabeto[4][0] = "E";
        Alfabeto[5][0] = "F";
        Alfabeto[6][0] = "G";
        Alfabeto[7][0] = "H";
        Alfabeto[8][0] = "I";
        Alfabeto[9][0] = "J";
        Alfabeto[10][0] = "K";
        Alfabeto[11][0] = "L";
        Alfabeto[12][0] = "M";
        Alfabeto[13][0] = "N";
        Alfabeto[14][0] = "O";
        Alfabeto[15][0] = "P";
        Alfabeto[16][0] = "Q";
        Alfabeto[17][0] = "R";
        Alfabeto[18][0] = "S";
        Alfabeto[19][0] = "T";
        Alfabeto[20][0] = "U";
        Alfabeto[21][0] = "V";
        Alfabeto[22][0] = "W";
        Alfabeto[23][0] = "X";
        Alfabeto[24][0] = "Y";
        Alfabeto[25][0] = "Z";
        
        Alfabeto[0][1] = "65";
        Alfabeto[1][1] = "66";
        Alfabeto[2][1] = "67";
        Alfabeto[3][1] = "68";
        Alfabeto[4][1] = "69";
        Alfabeto[5][1] = "70";
        Alfabeto[6][1] = "71";
        Alfabeto[7][1] = "72";
        Alfabeto[8][1] = "73";
        Alfabeto[9][1] = "74";
        Alfabeto[10][1] = "75";
        Alfabeto[11][1] = "76";
        Alfabeto[12][1] = "77";
        Alfabeto[13][1] = "78";
        Alfabeto[14][1] = "79";
        Alfabeto[15][1] = "80";
        Alfabeto[16][1] = "81";
        Alfabeto[17][1] = "82";
        Alfabeto[18][1] = "83";
        Alfabeto[19][1] = "84";
        Alfabeto[20][1] = "85";
        Alfabeto[21][1] = "86";
        Alfabeto[22][1] = "87";
        Alfabeto[23][1] = "88";
        Alfabeto[24][1] = "89";
        Alfabeto[25][1] = "90";
        String pal = null;
        int dato;
        String nombre;
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        miArbol.agregarNodo(89, "Y");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(90, "Z");
        miArbol.agregarNodo(85, "U");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(76, "L");
        miArbol.agregarNodo(66, "B");
        miArbol.agregarNodo(84, "T");
        miArbol.agregarNodo(72, "H");
        miArbol.agregarNodo(71, "G");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(83, "S");
        
        System.out.println("Letras del alfabeto");
   
    for(int i=0;i<26;i++){
    System.out.println(Alfabeto[i][0]+" = "+Alfabeto[i][1]);
    }
        
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()) {
            miArbol.preOrden(miArbol.raiz);
        }
    }
}
